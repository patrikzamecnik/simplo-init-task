<?php
namespace App\Http;

use Spatie\Robots\Robots;

class Analyze
{
    public function get_status_code($header) {
        return $header[9].$header[10].$header[11];
    }

    public function get_http_version($header) {
        return 'HTTP/'.$header[5].'.'.$header[7];
    }

    public function get_http_2_support($headers) {
        $http_supported = 'Support HTML/2.0: false, actual version is '.$this->get_http_version($headers[0]);
        if (strpos($headers[0], 'HTTP/2.0') !== false)
            $http_supported = 'Support HTML/2.0: true, actual version is '.$this->get_http_version($headers[0]);
        return $http_supported;
    }

    public function get_webp_support($headers) {
        $webp_supported = 'Image/Webp: Not supported,';
        if(strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false ) // Server-side only..maybe browser detection could help.
            $webp_supported = 'Image/Webp: Supported';
        return $webp_supported;
    }

    public function get_gzip_support($headers) {
        $gzip = '';
        foreach ($headers as $item) {
            if (strpos($item, 'gzip') !== false)  // We are looking for Content-Encoding: gzip. Gzip is good enough keyword for this.
                $gzip = 'Gzip: Supported,';
            else
                $gzip = 'Gzip: Not supported,';

        }
        return $gzip;
    }

    public function get_x_robots_tag_support($headers) {
        $x_robots_tag = '';
        foreach ($headers as $item) {
            if (strpos($item, 'X-Robots-Tag') !== false)  // We are looking for something like X-Robots-Tag: noindex.
                $x_robots_tag = 'X-Robots-Tag: Supported,';
            else
                $x_robots_tag = 'X-Robots-Tag: Not set,';
        }
        return $x_robots_tag;
    }

    public function get_robots_meta_tag_support($tags) {
        $robots_meta_tag = '';
        if (array_key_exists('robots', $tags) == true)
            $robots_meta_tag = 'robots meta tag: '.$tags['robots'].',';
        else
            $robots_meta_tag = 'robots meta tag: Not set,';
        return $robots_meta_tag;
    }

    public function get_robots_txt_support($input) {
        $robots_txt = Robots::create();
        $robots_txt_index = 'robots.txt: Cannot be indexed.';
        if($robots_txt->mayIndex($input))
            $robots_txt_index = 'robots.txt: May be indexed.';
        return $robots_txt_index;
    }
}