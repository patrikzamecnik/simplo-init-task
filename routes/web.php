<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Analyze;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/response', function() {
    $input = \Illuminate\Support\Facades\Input::input('url');
    $data = array();
    if ($input !== null && filter_var($input, FILTER_VALIDATE_URL)) {
        $analyze = new Analyze();

        // Get headers
        $headers = get_headers($input);
        // Get meta tags
        $tags = get_meta_tags($input);

        /*$browser = get_browser(null, true);
        if (array_key_exists('browser_name_pattern', $browser))
            echo $browser['browser_name_pattern'];*/

        $data = [
            'url' => $input,
            'status_code' => $analyze->get_status_code($headers[0]),
            'http2' => $analyze->get_http_2_support($headers),
            'http' => $analyze->get_http_version($headers[0]),
            'webp' => $analyze->get_webp_support($headers),
            'gzip' => $analyze->get_gzip_support($headers),
            'robots_meta_tag' => $analyze->get_robots_meta_tag_support($tags),
            'robots_txt' => $analyze->get_robots_txt_support($input),
            'x_robots_tag' => $analyze->get_x_robots_tag_support($headers)
        ];
    }
    return view('/response', $data);
});
